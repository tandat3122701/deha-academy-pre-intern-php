-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 26, 2024 lúc 04:33 PM
-- Phiên bản máy phục vụ: 8.4.0
-- Phiên bản PHP: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `curd`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `product_id` int NOT NULL,
  `product_maker` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_price` int NOT NULL,
  `product_img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`product_id`, `product_maker`, `product_name`, `product_price`, `product_img`) VALUES
(4, 'Nike', 'Men\'s Air Jordan 4 ', 190, './image/demo1.webp'),
(5, 'Nike', 'Men\'s Air Jordan 1 Retro Low OG', 145, './image/demo2.webp'),
(7, 'Adidas', 'jd4', 101, './image/demo4.webp'),
(8, 'Nike', 'jd5', 1, './image/demo5.webp'),
(9, 'Adidas', 'jd5', 101, './image/demo6.webp'),
(10, 'New Balance', 'e900', 101, './image/demo7.webp'),
(12, 'Adidas', 'Men\'s Samba MN', 120, './image/demo9.webp'),
(13, 'Adidas', 'Men\'s Samba OG', 90, './image/sambaog.webp'),
(14, 'New Balance', 'Moonrock', 140, './image/moonrock.webp'),
(15, 'Nike', 'Men\'s Dunk Low Retro', 100, './image/retrohigh.webp'),
(16, 'Adidas', 'Men\'s Samba MN', 100, './image/a1.webp'),
(17, 'Nike', 'Men\'s Dunk Low Retro', 100, './image/AURORA_DD1391-101_PHSRH000-2000.webp'),
(18, 'ADIDAS', 'Wales Bonner SL76', 100, './image/ADIDAS_1600000598351_5484.webp'),
(19, 'Adidas', 'Men\'s Superstar GTX', 110, './image/ADIDAS_1600000588839_3703.webp'),
(20, 'Adidas', 'Men\'s Superstar 82', 120, './image/ADIDAS_1600000592160_1475.webp'),
(21, 'Adidas', 'Clot Superstar By Edison Chen', 170, './image/ADIDAS_1600000605228__5587.webp'),
(22, 'New Balance', 'Men\'s M1906RGR', 140, './image/M1906RGR_2_1.webp'),
(23, 'New Balance', 'Men\'s UWRPDGD', 155, './image/UWRPDGD_2_1.webp'),
(24, 'New Balance', 'Men\'s U991LG2', 220, './image/NEW_BALANCE__5305.webp'),
(25, 'New Balance', 'Men\'s M1906RV1', 150, './image/M1906RRC_2.webp');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES
(7, 'tandat', 'tandat2106tn@gmail.com', '4297f44b13955235245b2497399d7a93'),
(8, 'tandat1', 'tandat2106t1n@gmail.com', '4297f44b13955235245b2497399d7a93'),
(9, 'tandat2', 'tandat21061tn@gmail.com', '4297f44b13955235245b2497399d7a93'),
(10, 'tandat3', 'tandat2106t11n@gmail.com', '4297f44b13955235245b2497399d7a93'),
(11, 'tandat6', 'tandat210611n@gmail.com', '4297f44b13955235245b2497399d7a93'),
(12, 'tandat8', 'dat@gmail.com', '4297f44b13955235245b2497399d7a93'),
(13, 'dat', 'dat123@gmail.com', '4297f44b13955235245b2497399d7a93'),
(14, 'tandat123', 'si22701@edu.aomori-u.ac.jp', '4297f44b13955235245b2497399d7a93'),
(15, 'tandataaa', 'dat111@gmail.com', '4297f44b13955235245b2497399d7a93'),
(16, 'tandat4123', 'tandat21062tn123@gmail.com', '4297f44b13955235245b2497399d7a93');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
