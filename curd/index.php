<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Curd User</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container text-center">
      
        <h1 class="mt-5">Xin chào! Hãy chọn mục chọn mục cần quản lí</h1>
        <div class="d-flex justify-content-center">
          <a href="./Product/index.php" class="btn btn-xl btn-primary mx-4 d-flex align-items-center">
            <i class="fas fa-shopping-cart fa-2x mr-5"></i>
            <span class="font-weight-bold">Products</span>
          </a>
          <a href="./User/index.php" class="btn btn-xl btn-primary mx-4 d-flex align-items-center">
            <i class="fas fa-users fa-2x mr-5"></i>
            <span class="font-weight-bold">Users</span>
          </a>
        </div>
    </div>
</body>
</html>