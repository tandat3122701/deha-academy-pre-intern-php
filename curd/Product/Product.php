<?php
include_once __DIR__ .'/../model/DB.php';
include_once __DIR__ .'/../model/helper.php';

class Product {
    static public function all() {
        $sql = "SELECT * FROM products";
        $products = DB::execute($sql);
        return $products;
    }

    static public function getProductsByMakerAndLimit($maker, $search, $start, $perPage) {
        $sql = "SELECT * FROM products WHERE 1=1";
        $params = [];

        if (!empty($maker)) {
            $sql .= " AND product_maker = :maker";
            $params['maker'] = $maker;
        }

        if (!empty($search)) {
            $sql .= " AND product_name LIKE :search";
            $params['search'] = "%$search%";
        }

        $sql .= " LIMIT $start, $perPage";
        $products = DB::execute($sql, $params);
        return $products;
    }

    static public function countProducts($maker, $search) {
        $sql = "SELECT COUNT(*) FROM products WHERE 1=1";
        $params = [];

        if (!empty($maker)) {
            $sql .= " AND product_maker = :maker";
            $params['maker'] = $maker;
        }

        if (!empty($search)) {
            $sql .= " AND product_name LIKE :search";
            $params['search'] = "%$search%";
        }

        $count = DB::execute($sql, $params);
        return $count[0]['COUNT(*)'];
    }

    static public function create($dataCreate) {
        $sql = "INSERT INTO products (product_maker, product_name, product_price, product_img)
                VALUES (:product_maker, :product_name, :product_price, :product_img)";
        $product = DB::execute($sql, $dataCreate);
        return count($product) > 0 ? $product[0] : [];
    }

    static public function find($product_id) {
        $sql = "SELECT * FROM products WHERE product_id = :product_id";
        $dataFind = ['product_id' => $product_id];
        $product = DB::execute($sql, $dataFind);
        return count($product) > 0 ? $product[0] : [];
    }

    static public function update($dataUpdate) {
        $sql = "UPDATE products
                SET product_maker = :product_maker,
                    product_name = :product_name,
                    product_price = :product_price,
                    product_img = :product_img
                WHERE product_id = :product_id";
        DB::execute($sql, $dataUpdate);
    }

    static public function destroy($product_id) {
        $sql = "DELETE FROM products WHERE product_id = :product_id";
        $dataDelete = ['product_id' => $product_id];
        DB::execute($sql, $dataDelete);
    }
}