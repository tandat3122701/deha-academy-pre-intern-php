<?php
include_once "Product.php";

$id=null;
$product=null;
if($_GET['product_id']){
    $product_id=$_GET['product_id'];
    $product=Product::find($product_id);
    
}else{
    header("location:./index.php");
}



//xử lý update
if (isset($_POST["edit"])) {
    $errors=validate($_POST,['product_maker','product_name','product_price','product_img']);
    if(count($errors)<1){
    $dataUpdate=[
        'product_id'=>$product['product_id'],
        'product_maker'=>$_POST['product_maker'],
        'product_name'=>$_POST['product_name'],
        'product_price'=>$_POST['product_price'],
        'product_img'=>'./image/'.$_POST['product_img'],

    ];
    $product=Product::update($dataUpdate);
    $_SESSION['message']='Update success';
    $errors=[];
    header("location:./index.php");
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Create Product</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div>
        <h1>Edit Product</h1>
    </div>
    <div>
        <form method="post">
        <div class="mb-3">
            <label  class="form-label">Maker name</label>
            <input type="text" name="product_maker" class="form-control">
            <div class="text-danger">
            <?php echo isset($errors['product_maker'])? ($errors['product_maker']):"" ?>
            </div>
        </div>
        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" name="product_name" >
            <div  class="text-danger">
            <?php echo isset($errors['product_name'])? ($errors['product_name']):"" ?>
            </div>
        </div>
        <div class="mb-3">
            <label for="price" class="form-label">Price</label>
            <input type="text" class="form-control" name="product_price" >
            <div  class="text-danger">
            <?php echo isset($errors['product_price'])? ($errors['product_price']):"" ?>
            </div>
        </div>
        <div class="mb-3">
            <label for="img" class="form-label">Image</label>
            <input type="text" class="form-control" name="product_img" >
            <div  class="text-danger">
            <?php echo isset($errors['product_img'])? ($errors['product_img']):"" ?>
            </div>
        </div>
        <button type="submit" class="btn btn-primary" name="edit">Update</button>
        </form>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>
</html>