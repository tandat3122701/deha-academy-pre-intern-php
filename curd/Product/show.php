<?php
include_once "Product.php";

$product_id=null;
$product=null;
if($_GET['product_id']){
    $product_id=$_GET['product_id'];
    $product=Product::find($product_id);
    
}else{
    header("location:./index.php");
}
?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Show Product</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
        <div>
            <h1>Product check</h1>
        </div>
        <?php if($product) {?>
            <h1>User infomation :</h1>
            <h3>ID: <?= $product['product_id']?></h3>
            <h3>Maker: <?= $product['product_maker']?></h3>
            <h3>Name: <?= $product['product_name']?></h3>
            <h3>Price: <?= '$'.$product['product_price']?></h3>
            
            
        <?php }else{?>
            <h1>Product is not found!!</h1>
        <a href="./index.php" class="btn btn-primary">Back to list</a>
        
        <?php }?>

    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
  </body>
</html>