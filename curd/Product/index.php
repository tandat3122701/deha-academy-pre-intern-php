<?php
    include_once "Product.php";
    $products=Product::all();

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Curd Product</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  </head>

  
  <body>
    <div class="container">
        <div>
            <h1>Product list</h1>
        </div>
        <?php if(isset($_SESSION['message'])) {?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <p>
                <?php echo ($_SESSION['message']); unset($_SESSION['message']) ?>
            </p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>       
        <?php }?>


        

        <a href="./create.php" class="btn btn-primary">Create</a>

        <div>
            <?php if (count($products)>0){?>
            <table class="table">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Maker name</th>
                <th scope="col">Name</th>
                <th scope="col">Price</th>
                <th scope="col">Image</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($products as $product){?>
                <tr>
                <th scope="row"><?= $product['product_id']?></th>
                <td><?= $product['product_maker']?></td>
                <td><?= $product['product_name']?></td>
                <td><?= $product['product_price']?></td>
                <td><img src="./../../web/<?= $product['product_img']?>" alt="product_img" style="width: 80px;"></td>
                <td>
                    <a href="./show.php?product_id=<?=$product['product_id']?>" class="btn btn-info">Show</a>
                    <a href="./edit.php?product_id=<?=$product['product_id']?>" class="btn btn-warning">Edit</a>
                    <form action="./delete.php" method="post" id="formDelete-<?=$product['product_id']?>">
                        <input type="hidden" name="product_id" value="<?= $product['product_id']?>">
                        <button type="button"product_id="<?=$product['product_id']?>"  class="btn btn-danger btn-delete">Delete</button>
                    </form>
                </td>
                </tr>
            <?php } ?>
            </tbody>
            </table>
            <?php }else{?>
                <h2>NO DATA!!</h2>
            <?php } ?>
        </div>
            

    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <script>
        let deleteBtns=document.querySelectorAll('.btn-delete');
        deleteBtns.forEach(function (item){
            item.addEventListener('click',function(event){
                if(confirm("Delete product")){
                let product_id=this.getAttribute('product_id');
                document.querySelector('#formDelete-'+product_id).submit();
                }

                
            })
        })
    </script>
</body>
</html>