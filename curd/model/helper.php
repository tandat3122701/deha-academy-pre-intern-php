<?php
function validateField($request,$key){
    return isset($request[$key]) && $request[$key] != "" ?"": "$key is required!";
}
function validate($request,$key){
    $result=[];
    foreach($key as $key){
        $error = validateField($request,$key);
        if($error){
            $result[$key]=$error;
        }
    }

    return $result;
}
?>