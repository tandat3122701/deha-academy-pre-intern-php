<?php
session_start();
class DB{
    static protected $connention;
    const DB_TYPE ='mysql';
    const DB_HOST = 'localhost';
    const DB_NAME = 'curd';
    const USER_NAME = 'root';
    const USER_PASSWORD = 'tandat2106Aa';
    static public function getConnection(){
        if(static::$connention == null){
            try{
                static::$connention = new PDO(self::DB_TYPE.":host=".self::DB_HOST.";dbname=".self::DB_NAME, self::USER_NAME, self::USER_PASSWORD);
            }catch(PDOException $exception){
                throw new Exception("connection fail");
            }
        }
        return static::$connention;
    }

    static public function execute($sql,$data=[]){
        $statement = DB::getConnection()->prepare($sql);
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $statement->execute($data);
        $result=[];
        while($item = $statement->fetch()) {
            $result[]=$item;
        }
        return $result;
        
        
        
    }
}