<?php
include_once "User.php";



//xử lý create
if (isset($_POST["create"])) {
    $errors=validate($_POST,['name','email','password']);
    if(count($errors)<1){
    $dataCreate=[
        'name'=>$_POST['name'],
        'email'=>$_POST['email'],
        'password'=>md5($_POST['password']),
    ];

    $user=User::create($dataCreate);
    $_SESSION['message']='Create success';
    $errors=[];
    header("location:./index.php");
    }
}else{
    $errors=[];
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Create User</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div>
        <h1>Create User</h1>
    </div>
    <div>
        <form method="post">
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Email address</label>
            <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
            <div id="emailHelp" class="text-danger">
            <?php echo isset($errors['email'])? ($errors['email']):"" ?>
            </div>
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Name</label>
            <input type="text" class="form-control" name="name" >
            <div  class="text-danger">
            <?php echo isset($errors['name'])? ($errors['name']):"" ?>
            </div>
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Password</label>
            <input type="password" class="form-control" name="password" id="exampleInputPassword1">
            <div  class="text-danger">
            <?php echo isset($errors['password'])? ($errors['password']):"" ?>
            </div>
        </div>
        <button type="submit" class="btn btn-primary" name="create">Create</button>
        </form>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>
</html>