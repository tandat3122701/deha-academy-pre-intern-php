<?php
    include_once "User.php";
    $users=User::all();

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Curd User</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
        <div>
            <h1>User list</h1>
        </div>
        <?php if(isset($_SESSION['message'])) {?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <p>
                <?php echo ($_SESSION['message']); unset($_SESSION['message']) ?>
            </p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>       
        <?php }?>


        

        <a href="./create.php" class="btn btn-primary">Create</a>

        <div>
            <?php if (count($users)>0){?>
            <table class="table">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">User name</th>
                <th scope="col">Email</th>
                <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $user){?>
                <tr>
                <th scope="row"><?= $user['id']?></th>
                <td><?= $user['name']?></td>
                <td><?= $user['email']?></td>
                <td>
                    <a href="./show.php?id=<?=$user['id']?>" class="btn btn-info">Show</a>
                    <a href="./edit.php?id=<?=$user['id']?>" class="btn btn-warning">Edit</a>
                    <form action="./delete.php" method="post" id="formDelete-<?=$user['id']?>">
                        <input type="hidden" name="id" value="<?= $user['id']?>">
                        <button type="button"id="<?=$user['id']?>"  class="btn btn-danger btn-delete">Delete</button>
                    </form>
                </td>
                </tr>
            <?php } ?>
            </tbody>
            </table>
            <?php }else{?>
                <h2>NO DATA!!</h2>
            <?php } ?>
        </div>
            

    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <script>
        let deleteBtns=document.querySelectorAll('.btn-delete');
        deleteBtns.forEach(function (item){
            item.addEventListener('click',function(event){
                if(confirm("Delete user")){
                let id=this.getAttribute('id');
                document.querySelector('#formDelete-'+id).submit();
                }

                
            })
        })
    </script>
</body>
</html>