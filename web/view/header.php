<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>
<html lang="en">

<body>
  <header>
    <nav class="navbar navbar-expand-sm navbar-dark bg-dark pt-0 pb-0 mynav fixed-top">
        <div class="container-fluid lg-col">
          <a class="navbar-brand py-0" href="javascript:void(0)"><img src="image/logo.png" class="logo" alt=""></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="mynavbar">
            <ul class="navbar-nav me-auto">
                <li class="nav-item">
                    <a class="nav-link" href="?">All</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?maker=Nike">Nike</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?maker=Adidas">Adidas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?maker=New Balance">New Balance</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?maker=Fila">Fila</a>
                </li>
            </ul>

            
              
            
        <?php if (isset($_SESSION['id'])): ?>

          <button type="button" class="btn btn-primary mx-2 text-light" >
                <i class="fa fa-user"></i><strong> | <?php echo $_SESSION['name'] ?></strong>
            </button>
            <button type="button" class="btn btn-danger text-light" onclick="window.location.href='logout.php'">
                <i class="fa fa-sign-out-alt"></i><strong> | Logout</strong>
            </button>


            
        <?php else: ?>
            <button type="button" class="btn btn-success text-light" onclick="window.location.href='login.php'">
                <i class="fa fa-user"></i><strong> | Login</strong>
            </button>
            <button type="button" class="btn btn-primary mx-2 text-light" onclick="window.location.href='register.php'">
                <i class="fa fa-user-plus"></i><strong> | Đăng ký</strong>
            </button>
            
        <?php endif; ?>
    </div>
            </div>
        </div>
      </nav>
    </header>
  <div class="app  ">
      <div class="container-fluid banner">

       <div id="demo" class="carousel slide" data-bs-ride="carousel">
  <!-- Indicators/dots -->
          <div class="carousel-indicators">
            <button type="button" data-bs-target="#demo" data-bs-slide-to="0" class="active"></button>
            <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
            <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
          </div>
          <!-- The slideshow/carousel -->
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="image/baner1.png" alt="1" class="d-block w-100">
            </div>
            <div class="carousel-item">
              <img src="image/banner2.png" alt="2" class="d-block w-100">
            </div>
            <div class="carousel-item">
              <img src="image/banner3.png" alt="3" class="d-block w-100">
            </div>
          </div>
          <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
            <span class="carousel-control-prev-icon"></span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
            <span class="carousel-control-next-icon"></span>
          </button>
        
        </div>

      </div>

