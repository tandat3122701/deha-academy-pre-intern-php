
  
  <!-- new iteams -->
    <div class="container new position-relative overflow-hidden">
      <img src="image/new1.png" alt="">
      <h1 class="afterH1">
        <span>New Balance A900</span>
      </h1>
      <h2 class="afterH2">Only $90</h2>
      <h1 class="beforeH1">
        <span>Comming soon!</span>
      </h1>
      <img src="image/new2.png" class="position-absolute top-100 start-50 translate-middle new2" alt="">
      <button class="circle-button position-absolute" onclick="animateElements()">Click here</button>
      <button class="btn btn-light mt-5 px-2 reserveBtn" id="loadmore"><strong>Reserve now</strong></button>
    </div>


  <footer class="bg-light text-dark pt-5 pb-4">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <h5>About</h5>
          <p>Chúng tôi là một công ty chuyên cung cấp các sản phẩm chất lượng cao với giá cả hợp lý.</p>
        </div>
        <div class="col-md-4">
          <h5>Liên hệ</h5>
          <ul class="list-unstyled">
            <li><i class="fas fa-map-marker-alt"></i> 123 Đường ABC, Quận XYZ, TP. HCM</li>
            <li><i class="fas fa-phone"></i> (123) 456-7890</li>
            <li><i class="fas fa-envelope"></i> info@example.com</li>
          </ul>
        </div>
        <div class="col-md-4">
          <h5>Theo dõi chúng tôi</h5>
          <ul class="list-inline">
            <li class="list-inline-item"><a href="#" class="text-dark"><i class="fab fa-facebook"></i></a></li>
            <li class="list-inline-item"><a href="#" class="text-dark"><i class="fab fa-twitter"></i></a></li>
            <li class="list-inline-item"><a href="#" class="text-dark"><i class="fab fa-instagram"></i></a></li>
            <li class="list-inline-item"><a href="#" class="text-dark"><i class="fab fa-youtube"></i></a></li>
          </ul>
        </div>
      </div>
      <hr>
      <div class="text-center">
        <p>&copy; 2024 tandat.</p>
      </div>
    </div>
  </footer>
<script src="app.js"></script>
</body>
</html>