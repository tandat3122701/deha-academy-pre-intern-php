<div class="container">
          
  <?php if (!empty($products)) : ?>
      <h2 class="text-center mt-5">
        Danh sách sản phẩm
      </h2>
      
      <div class=" row g-4 mt-1" id="product">
      <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <form action="" method="GET" class="mb-3" id="search">
                  <div class="input-group search-group">
                      <input type="text" class="form-control" placeholder="Tìm kiếm theo tên sản phẩm" name="search" value="<?= isset($_GET['search']) ? $_GET['search'] : '' ?>">
                      <button class="btn btn-primary" type="submit">Tìm kiếm</button>
                  </div>
              </form>
          </div>
      </div>
      </div>


      <?php foreach ($products as $product){?>
      
        <!-- page 1 -->
        <div class="card col-sm-12 col-md-6 col-lg-3 pg1 " >
        <a href="./view/detail/detail.html" class="">
            <img class="card-img-top" src="<?= $product['product_img']?>" alt="Card image" style="width:100%">
            <div class="card-body">
              
              <h4 class="card-title"><?= $product['product_maker']?></h4>
              <h5 class="card-title"><?= $product['product_name']?></h5>
              <hr>
              <h5 class="card-text "><?= "$".$product['product_price']?></h5>
            </div>
          </a>
        </div>
      
      <?php } ?>

      <nav aria-label="Page navigation ">
          <ul class="pagination justify-content-center">
              <?php
              $maxDisplayedPages = 3;
              $halfDisplayedPages = floor($maxDisplayedPages / 2);
              $startPage = max(1, $page - $halfDisplayedPages);
              $endPage = min($totalPages, $startPage + $maxDisplayedPages - 1);

              if ($page > 1) {
                  echo '<li class="page-item"><a class="page-link" href="?maker=' . $maker . '&page=' . ($page - 1) . '">&laquo;</a></li>';
              }

              for ($i = $startPage; $i <= $endPage; $i++) {
                  echo '<li class="page-item' . ($i == $page ? ' active' : '') . '"><a class="page-link" href="?maker=' . $maker . '&page=' . $i . '">' . $i . '</a></li>';
              }

              if ($page < $totalPages) {
                  echo '<li class="page-item"><a class="page-link" href="?maker=' . $maker . '&page=' . ($page + 1) . '">&raquo;</a></li>';
              }
              ?>
          </ul>
      </nav>
  <?php else : ?>
    <div class="col-12 text-center my-5 ">
        <h2 class="bg-danger text-light py-2">Không có sản phẩm !</h2>
        <a class="btn btn-primary px-5 text-light" href="?">Trở về trang chính</a>
    </div>
  <?php endif; ?>

      </div>
      
        <script>
          window.onload = function() {
          var section1 = document.getElementById('product');
          section1.scrollIntoView();
          };
        </script>
      </div>

    </div>