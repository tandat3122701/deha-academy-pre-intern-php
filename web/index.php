<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="./view/style.css">
    <script src="./view/app.js"></script>
    <link rel="icon" type="image/x-icon" href="image/logo.png">
    <title>TD Sneaker</title>
</head>


<?php include_once "./view/header.php";
?>
<?php

include_once "./../curd/Product/Product.php";

$maker = isset($_GET['maker']) ? $_GET['maker'] : '';
$search = isset($_GET['search']) ? $_GET['search'] : '';
$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
$perPage = 8;
$startAt = $perPage * ($page - 1);

$products = Product::getProductsByMakerAndLimit($maker, $search,$startAt, $perPage);
$count = Product::countProducts($maker,$search);
$totalPages = ceil($count / $perPage);
?>
<?php include_once "./view/body.php"?>
<?php include_once "./view/footer.php"?>
