<?php
require '../curd/User/User.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$error = '';
$success = '';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = md5($_POST['password']);
    
    // Kiểm tra xem tên người dùng hoặc email đã tồn tại chưa
    $existingUser = User::findByNameOrEmail($name, $email);
    
    if ($existingUser) {
        $error = "Username or email already exists";
    } else {
        // Đăng ký người dùng mới
        $registerSuccess = User::register($name, $email, $password);
        
        if ($registerSuccess) {
            $success = "Registration successful. You can now log in.";
            // Không sử dụng sleep và header ở đây
        } else {
            $error = "Registration failed. Please try again.";
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script>
        // JavaScript để chuyển hướng sau 5 giây nếu đăng ký thành công
        function redirectToLogin() {
            setTimeout(function() {
                window.location.href = 'login.php';
            }, 5000); // 5000 milliseconds = 5 seconds
        }
    </script>
</head>
<body>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Register</h4>
                    </div>
                    <div class="card-body">
                        <?php if ($error !== ''): ?> 
                            <div class="alert alert-danger"><?php echo $error; ?></div> 
                        <?php endif; ?>
                        <?php if ($success !== ''): ?> 
                            <div class="alert alert-success">
                                <?php echo $success; ?>
                                <script>redirectToLogin();</script> <!-- Chạy JavaScript để chuyển hướng -->
                            </div> 
                        <?php endif; ?>
                        <form method="POST" action="register.php">
                            <div class="mb-3">
                                <label for="name" class="form-label">Username</label>
                                <input type="text" class="form-control" id="name" name="name" required>
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input type="email" class="form-control" id="email" name="email" required>
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">Password</label>
                                <input type="password" class="form-control" id="password" name="password" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Register</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
